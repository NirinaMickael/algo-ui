import { Medicament, Taquin } from "@/Screens";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { RootStackParamList } from "types_";
import BaseBottomTabs from "./BaseBottomTabs";

const StackScreen = createNativeStackNavigator<RootStackParamList>()
export default function Navigation() {
    return (
        <NavigationContainer>
            <BottomTabs />
        </NavigationContainer>
    )
}

const BottomTab = createBottomTabNavigator();
function BottomTabs() {
    return (
        <BottomTab.Navigator
            initialRouteName="taquin"
            tabBar={props => <BaseBottomTabs {...props} />}
        >
            <BottomTab.Screen name="taquin"
                component={Taquin}
                options={{ tabBarIcon: "apps" }}
            />
            <BottomTab.Screen
                name="medicament"
                component={Medicament}
                options={{
                    tabBarIcon: "medical"
                }}
            />
        </BottomTab.Navigator>
    )
}
// function RootNavigation() {
//     return (
//         <StackScreen.Navigator
//             screenOptions={{
//                 animationDuration: 0,
//                 animation: 'none',
//                 headerShown: false,
//             }}
//             initialRouteName="Root"
//         >
//             <StackScreen.Screen
//                 name="Root"
//                 component={Taquin}
//                 options={
//                     {
//                         title: "Taquin",
//                     }
//                 }
//             />
//         </StackScreen.Navigator>
//     )
// }
