import { Positon } from "../schema/Position"

enum CONST {
    ZERO = 0
}
type Matrix = number[][]
type Vector = number[]
const ACTION: Positon[] = [
    { x: 1, y: 0 },
    { x: -1, y: 0 },
    { x: 0, y: 1 },
    { x: 0, y: -1 }
]
const FINAL_STATE: Matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 0]
]
const CURRENT_STATE: Matrix = [
    [8, 2, 3],
    [1, 0, 4],
    [7, 6, 5]
]
type QUEUE = {
    vertice: Vector,
    path: number[][]
}


export { FINAL_STATE, CURRENT_STATE, ACTION, CONST, Matrix, QUEUE, Vector }