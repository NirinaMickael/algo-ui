import { Matrix } from "../Constant/Constant";

class Utils {
    constructor() {
        
    }
    AfficheMatrix(M : Matrix){
        for (let i = 0; i < M.length; i++) {
            let row = "";
            for (let j = 0; j < M[i].length; j++) {
                row += M[i][j] + " ";
            }
            console.log(row.trim());
        }
    }
}

export default new Utils();