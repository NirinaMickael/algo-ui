

import { ACTION, CONST, CURRENT_STATE, FINAL_STATE, Matrix, QUEUE, Vector } from "./src/Constant/Constant";
import { Positon } from "./src/schema/Position";

export interface result {
    bestPath: Matrix,
    time: number,
    cost: number
}
export class TaquinAlgo {
    grid !: Vector
    taille: number
    actions: Positon[]
    finalState: Vector;
    Cost: number = Infinity;
    constructor(taille: number) {
        this.taille = taille;
        this.actions = ACTION;
        this.finalState = this.finalStateGrid();
        // this.shuffle();
        this.grid = this.finalState;
        this.taille = taille;
    }
    private isCanMove(positon: Positon): boolean {
        if ((positon.x >= 0 && positon.x < this.taille) && (positon.y >= 0 && positon.y < this.taille)) {
            return true;
        } else {
            return false;
        }
    }
    public getNeighbors(state: Matrix): Matrix[] {
        let positionZero: Positon = this.getPositionZero(state);
        let neighbors: Matrix[] = [];
        for (const action of this.actions) {
            const nextPosition: Positon = {
                x: positionZero.x + action.x,
                y: positionZero.y + action.y
            }
            if (this.isCanMove(nextPosition)) {
                const currentState: Matrix = state.map(s => s.slice());
                const temp = currentState[nextPosition.x][nextPosition.y];
                currentState[nextPosition.x][nextPosition.y] = currentState[positionZero.x][positionZero.y];
                currentState[positionZero.x][positionZero.y] = temp;
                neighbors.push(currentState)
            }
        }
        return neighbors;
    }
    private getPositionZero(state: Matrix): Positon {
        for (let i = 0; i < this.taille; i++) {
            const j = state[i].indexOf(CONST.ZERO);
            if (state[i].indexOf(CONST.ZERO) != -1) {
                return {
                    x: i,
                    y: j
                }
            }
        }
        return { x: 0, y: 0 };
    };
    public BFS(): result {
        const queue: QUEUE[] = [{
            vertice: this.grid, path: [this.grid]
        }];
        const visited = new Set<string>();
        visited.add(JSON.stringify(this.grid));
        let i = 0;
        let startTime = performance.now();
        while (queue.length) {
            const { vertice, path } = queue.shift() as QUEUE;
            if (JSON.stringify(vertice) == JSON.stringify(this.finalState)) {
                if (this.Cost > path.length) {
                    this.Cost = path.length;
                    const time = performance.now() - startTime;
                    console.log(time);
                    return {
                        bestPath: path,
                        time: time,
                        cost: path.length
                    };
                }
            }
            const neighbors = this.getNeighbors(this.toMatrix(vertice, this.taille));
            for (const neighbor of neighbors) {
                if (!visited.has(JSON.stringify(neighbor))) {
                    queue.push(
                        { vertice: neighbor.flat(), path: [...path, neighbor.flat()] }
                    )
                    visited.add(JSON.stringify(neighbor));
                }
            }
        }
        return {
            bestPath: [],
            time: 0,
            cost: 0
        }
    }
    toMatrix(array: Vector, n: number): Matrix {
        const resultados: Matrix = [];
        for (var i = 0; i < array.length; i += n) {
            resultados.push(array.slice(i, i + n))
        }
        return resultados;
    }
    public shuffle() {
        const array = this.finalState;
        let suffle_ = [...array].sort(() => Math.random() - 0.5);
        while(!this.isSolvable(suffle_)){
            suffle_ =[...array].sort(() => Math.random() - 0.5)
        }
        console.log(suffle_)
        this.grid = suffle_;
    };
    public isSolvable (flattenedPuzzle: Vector) {
        let inversions = 0;
        for (let i = 0; i < flattenedPuzzle.length; i++) {
          for (let j = i + 1; j < flattenedPuzzle.length; j++) {
            if (
               flattenedPuzzle[i] &&
               flattenedPuzzle[j] &&
               flattenedPuzzle[i] > flattenedPuzzle[j]
             ) {
               inversions++;
              }
         }
       }
        return inversions % 2 === 0;
    };
    private finalStateGrid(): Vector {
        const initState: Vector = []
        for (let i = 1; i < this.taille * this.taille; i++) {
            initState.push(i);
        }
        initState.push(0);
        return initState;
    }
    public countInversions(arr: Vector) {
        let inversions = 0;
        for (let i = 0; i < arr.length - 1; i++) {
            for (let j = i + 1; j < arr.length; j++) {
                // if (arr[i] != 0 && arr[j] != 0) {
                if (arr[j] > arr[i]) {
                    inversions++;
                    // }
                }
            }
        }
        return inversions;
    }

}
// (async () => {
//     try {
//         const result = await taquin.BFS();
//         console.log(result)
//     } catch (error) {
//         console.log(error)
//     }
// })()