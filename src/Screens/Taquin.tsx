import { COLORS } from "@/Constants/_Color";
import { TaquinAlgo, result } from "@/Utils/algo/main";
import { CURRENT_STATE, FINAL_STATE } from "@/Utils/algo/src/Constant/Constant";
import { BaseButton } from "@/components/atoms/BaseButton";
import Boxs from "@/components/molecules/Boxs";
import { Bfs } from "@/services/Taquin/taquin";
import { MaterialIcons, AntDesign } from "@expo/vector-icons";
import { useEffect, useRef, useState } from "react";
import { View, StyleSheet, Text, TouchableOpacity } from "react-native";


export default function Taquin() {
  const taquinInstancev = useRef(new TaquinAlgo(3)).current;
  const [boardState, setBoardState] = useState(taquinInstancev.grid);
  const [solutions, setSolution] = useState<result>(
    {
    bestPath: CURRENT_STATE,
    cost: 0,
    time: 0
   }
  );
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    taquinInstancev.shuffle();
    setBoardState(taquinInstancev.grid)
  }, [])

  const randomState = async () => {
    taquinInstancev.shuffle();
    setBoardState(taquinInstancev.grid);
    setSolution({
      bestPath: [taquinInstancev.grid],
      cost: 0,
      time: 0
    })
  };

  const resoudre = async () => {
    try {
      setLoading(true);
      const response = (await Bfs(boardState)).data;
        setSolution(response);
        setLoading(false);
    } catch (error: any) {
      setSolution(error);
      setLoading(false);
    }
  };

  const run = () => {
    solutions.bestPath.forEach((solution, index) => {
      setTimeout(() => {
        setBoardState(solution as any)
      }, index * 1000);
    });
  }
  return (
    <View style={styles.container}>
      <View
        style={
          styles.boxContainer
        }
      >
        <View
          style={
            styles.headerTaquin
          }
        >
          <View>
            <Text>Time : {solutions.time}ms </Text>
          </View>
          <View>
            <Text> Cost  : {solutions.cost}  </Text>
          </View>
          <TouchableOpacity
            onPress={() => randomState()}
          >
            <AntDesign name="sync" size={20} color="black" />
          </TouchableOpacity>
        </View>
        <Boxs gird={boardState} />
        <View
          style={
            styles.buttonContainer
          }
        >
          <BaseButton
            onPress={() => { resoudre() }}
            disabled={false}
            loading={loading}
            title={"Resolve"}
            style={{
              backgroundColor: COLORS.PRIMARY,
              ...styles.buttonStyle
            }}
            iconLeft={
              <MaterialIcons name="sync-problem" size={25} color={COLORS.GRAY} />
            }
          />
          <BaseButton
            onPress={() => { run() }}
            disabled={false}
            loading={false}
            title={"Run"}
            style={{
              backgroundColor: COLORS.SECONDARY
              , ...styles.buttonStyle
            }}
            iconLeft={
              <MaterialIcons name="play-arrow" size={25} color={COLORS.GRAY} />
            }
          />
        </View>
        <View
          style={
            {
              padding: 10,
              display: "flex",
              justifyContent: "center",
            }
          }
        >
          <Text
            style={
              {
                color: "red"
              }
            }
          >
            {
              solutions.bestPath.length == 0 ? "No solution" : ""
            }
          </Text>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
  },
  boxContainer: {
    borderWidth: 1,
    borderColor: "#aaa2",
    width: "90%",
    left: "5%",
    padding: 10
  },
  buttonContainer: {
    flexDirection: "row",
    width: "90%",
    gap: 10,
    marginTop: 15,
    alignSelf: "center"
  },
  buttonStyle: {
    width: "49%",
  },
  headerTaquin: {
    width: "90%",
    left: "5%",
    flexDirection: "row",
    display: "flex",
    justifyContent: "space-between",
    padding: 6
  }
});
