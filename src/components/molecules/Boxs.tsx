import { StyleSheet, View } from "react-native"
import Box from "../atoms/Box"
import { useEffect, useState } from "react"
// TaquinInstance = new Taquin()
type BoxsProps = {
    gird: number[]
}
export default function Boxs(boxsProps: BoxsProps) {
    return (
        <View style={[styles.boxs]} >
            {
                boxsProps.gird.map((block, index) => {
                    const action = () => {
                        console.log(index);
                    }
                    return (
                        <Box num={block} onPress={action} key={index} />
                    )
                })
            }
        </View>
    )
}

const styles = StyleSheet.create({
    boxs: {
        // // flex: 4,
        // width: "90%",
        flexDirection: "row",
        // backgroundColor:"red",
        justifyContent: "center",
        alignContent: "center",
        gap: 2,
        flexWrap: "wrap",
    }
})