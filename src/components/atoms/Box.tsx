import { COLORS } from "@/Constants/_Color";
import { View, Text, StyleSheet, TouchableOpacity, Animated } from "react-native";
import { TBox } from "types_";
type boxProps = {
    num: number,
    onPress: () => void
}
export default function Box(BoxProps: boxProps) {
    return (
        <TouchableOpacity

            onPress={() => BoxProps.onPress()}
            style={[styles.BoxContainer, BoxProps.num == 0 ?
                {
                    backgroundColor: COLORS.GRAY,
                } :
                {
                    backgroundColor: COLORS.TERNAIRY,
                }
            ]}>
            <Text
                style={
                    [
                        styles.label,
                        BoxProps.num == 0 ?
                            {
                                color: COLORS.TERNAIRY,
                            } :
                            {
                                color: COLORS.GRAY,
                            }
                    ]
                }
            >
                {BoxProps.num}
            </Text>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    BoxContainer: {
        width: "30%",
        height: 100,
        justifyContent: "center",
        alignItems: "center",
        // shadowColor: "black",
        // shadowOffset: { width: -2, height: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        // borderWidth: 0.2,
    },
    label: {
        fontSize: 15,
    },
})