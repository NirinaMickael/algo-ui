import { COLORS } from "@/Constants/_Color"
import { ActivityIndicator, StyleSheet, Text, TouchableOpacity } from "react-native"
type ButtonProps = {
    onPress: () => void,
    style?: Object,
    textStyle?: Object,
    disabled?: boolean,
    loading?: boolean,
    title: string,
    activeOpacity?: number,
    iconLeft?: any,
    iconRight?: string
}
export function BaseButton(buttonProps: ButtonProps) {
    return (
        <TouchableOpacity

            onPress={() => { buttonProps.onPress() }}
            disabled={buttonProps.disabled}
            activeOpacity={buttonProps.activeOpacity}
            style={[
                buttonProps.disabled ? styles.disabledButton : styles.button, buttonProps.style
            ]}
        >

            {buttonProps.loading ? (
                <ActivityIndicator color="red" />
            ) : (
                <>
                    {buttonProps.iconLeft}
                    <Text style={[styles.buttonText, buttonProps.textStyle]}>{buttonProps.title}</Text>
                    {buttonProps.iconRight}
                </>
            )}
        </TouchableOpacity>
    )
};
const styles = StyleSheet.create({
    button: {
        backgroundColor: COLORS.TERNAIRY,
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 5,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
    },
    disabledButton: {
        backgroundColor: "grey",
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 5,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
    },
    buttonText: {
        color: "white",
        fontSize: 16,
        marginHorizontal: 5
    },
});
